﻿Exercicis de fitxers (05)
	Conceptes clau:
* Informació d’usuari
* Quotes i ocupació de disc.
* Buscar fitxers (nom, mida, propietari, tipus)
* Empaquetar contingut: tar
Ordres a treballar:
id, who, finger, chfn, chsh, .plan
Quota, du, du --max-depth, du -s, df
updatedb, locate
find, find --size, find --user, find --type
tar, tar -pP
Exercicis
________________
Primera part
1. Fer actiu el directori /var/tmp.

	cd /var/tmp

2. Identifica el ID i UID de l’usuari actual, de root i de guest.

	id a191545jj root guest

3. Llista els usuaris que han fet login en el sistema.

	who

4. Identifica el funcionament de l’ordre finger i executa-la.

	finger

	mostra la descripció emmagatzemada al geckos

5. Fes l’ordre finger per al teu isx, per a roor i per a guest.

	finger a191545jj guest root

6. Llistar els shells disponibles en el sistema: /etc/shells.

	cat /etc/shells

7. Idetifica quin shell tens assignat.

	finger a191545jj

	Apareix en el camp "Shell: "

8. Modificar les dades de l’usuari guest:
   1. Establir un nom, empresa i telèfon. 

	chfn a191545jj

   2. Assignar-li el shell sh.

	sudo chsh -s /bin/sh guest	

   3. Per a què servei el fitxer .plan



Segona part
1. Fer actiu el directori home de l’usuari.

	cd

2. Realitza l’ordre quaota i identifica la informació que mostra.

	Mostra l'ocupacio de disc d'un usuari i els seus limits

3. Calcula l’ocupació d’espai del teu home amb l’ordre du, du -s, du -c.

	du
	du -s  # Simplifica el resultat no mostrant els directoris
	du -c  # Mostra el gran total de tot l'espai en cas de donar més d'un argument.

4. Llista l’ocupació del directori /var/tmp/esport. Calcula el total d’ocupació d’aquest directori.

	du /var/tmp/esport
	du -c /var/tmp/esport

5. Ídem mostrant els subtotals de cada sots directori que conté (/var/tmp/esport). És a dir, mostrar un nivell de profunditat.

	du -d 1 /var/tmp 2> /dev/null

6. Fer el mateix del directori home de l'usuari.

	du -d 1 

7. També fer-ho de l’arrel del sistema.

	du -d 1 / 2> /dev/null

8. Lllistar l'ocupació de disc del directori home l'usuari agrupant fins a dos nivells de profunditat.

	du -d 2 

9. Mostra l’ocupació d’espai del disc amb df -h.

	df -h

10. Mostra l’ocupació d’espai del disc de les particions ext4.

	df -h -t ext4

Tercera part
1. Fer actiu el directori /var/tmp.

	cd /var/tmp

2. Realitza l'ordre updatedb i esbrina què fa.

	sudo updatedb 
	Actualitza la base de dades de l'ordre mlocate

3. Localitza tots els fitxers de nom vmlinuz.

	locate vmlinuz

4. Localitza el fitxer services.

	locate services

5. Localitza els fitxers de nom ifcfg.

	locate ifcfg

6. Localitza el fitxer carta03. Ho fa? Perquè?

	Perquè locate busca tot el que contingui el nom especificat.

7. Identifica el funcionament de l’ordre find i troba tots els fitxers de més de 1M del directori /boot.

	find /boot/ -size +1M
	Mostra tots els fitxers amb el criteri especificat

8. Amb l’ordre find i troba tots els fitxers de menys de 4M del directori /boot.

	find /boot/ -size -4M

9. Amb l’ordre find i troba tots els fitxers de 1M a 4M del directori /boot.

	find /boot -size +1M -size -4M

10. Localita de /var/tmp tots els fitxers de propietat teva.

	find /var/tmp -user a191545jj

11. Localita de /var/tmp tots els fitxers de propietat de root.

	find /var/tmp -user root

12. Identifica els fitxers de /boot de nom initrd, amb find.

	find /boot -name initrd*

13. Identifica els fitxers de /boot de nom .cfg amb find.

	find /boot -name *.cfg

14. Usant find llista els fitxers de tipus directori de /etc i de /tmp i de /boot.

	find /etc /tmp /boot -type d -ls

15. Usant find llista els fitxers de tipus link de /etc/systemd/system i de /tmp

	find /etc/systemd/system /tmp -type l -ls

16. Usant find llista els fitxers de tipus file del teu home.

	find ~ -type f -ls

17. Listar tots els fitxers del directori /var/tmp més nous que una data en concret.



18. Listar tots els fitxers del directori /var/tmp amb data de fa deu dies fins ara.



19. Listar tots els fitxers del directori /var/tmp amb data anterior a fa 72 hores.



Quarta part
1. Empaqueta el contingut de /var/tmp a un fitxer de /tmp anomenat paquet.tar.

	tar cvf /tmp/paquet.tar /var/tmp

2. Llistar el contingut de paquet. Són rutes absolutes o relatives?

	tar -tf /tmp/paquet.tar

3. Extreure el contingut de paquet dins de /tmp/newdata.

	tar -xvf /tmp/paquet.tar -C /tmp/newdata/

4. Extreure només els fitxers txt i desar-los a /tmp/prova.

	tar -xvf /tmp/paquet.tar -C /tmp/prova --wildcards *.txt

5. Empaquetar el contingut de /tmp/newdata en un fitxer  anomenat paquet-absolute.tar al directori /tmp. Cal que el paquet es generi mantenint les rutes absolutes dels fitxers origens.

	tar cvfP /tmp/paquet-absolute.tar /tmp/newdata/
	
6. Llistar el contingut de paquet. Són rutes absolutes o relatives?

	tar tf paquet-absolute.tar
	Són rutes absolutes.

7. Esborrar el directori /tmp/newdata i tot el que conté.

	rm -rf /tmp/newdata

8. Desempaquetar paquet-absolute.tar  indicant com a destí /tmp/prova, però observar on es generen els elements desempaquetats. Aquí dins o al seu path absolut?

	tar xvf paquet-absolute.tar -C /tmp/prova/

9. Repatir l’exercici anterior provant les diferències de desenpaquetar amb l’opció -p o -P.



10. Empaquetar el contingut de /var/tmp generant un paquet comprimit. Llistar el contingut i extreure’n només els fitxers txt.

	tar czvf paquet.tgz /var/tmp
	tar tf paquet.tgz 
	tar xzvf paquet.tgz --wildcards *.txt

11. Empaquetar el contingut de /var/tmp generant un paquet comprimit amb bzip. Llistar el contingut i extreure’n només els fitxers txt.

	tar cjvf paquet.tbz2 /var/tmp
	tar tf paquet.tbz2 
	tar xjvf paquet.tbz2 --wildcards *.txt
