# LPIC-1


## 103.5 (4) Create, monitor and kill processes


### Alternate exercises:

1. Mostrar tots els processos del sistema.
```
ps ax
ps -ef
```
1. Mostrar tot l'arbre de processos incloent el pid i la ordre. 
```
pstree -apl
```
3. Prova les ordres: ps, ps a, ps x, ps ax, ps -fu pere, ps -Fu pere.
```
Provat
```
4. Llistar els processos  fent un llistat llarg. el PID i el PPID.
```
ps -l
```
5. Entrar en un subshell i fer un llistat llarg dels processos. 
```
bash
ps -l
```
6. Identificar el PID del procés pare del shell actual.
```
0 S 204955   6861    5162  0  80   0 -  4427 -      pts/0    00:00:00 bash
```
7. Identifica el PID del procés init usant l'ordre pidof.
```
pidof init
PID 1
```
8. Identifica el pid del servei d'impressió cupsd amb l'ordre pidof.
```
pdof cupsd
PID 808
```
9.  Usant l'ordre pgrep llista els processos de l'usuari root.
```
pgrep -lu root
```
10. Usant l'ordre pgrep localitza el procés init.
```
pgrep systemd
```
11. Utilitzant l'ordre fuser per saber quins processos utilitzen el directori /tmp.
```
cd /tmp
fuser -a /tmp

/tmp:                 4511c

És el PID del bash, la "c" significa directori actiu.
```
12. Llista tots els senyals de l’ordre kill.
```
kill -l
```
13. Genera un procés sleep 10000 i mata’l amb kill.
```
sleep 100000 &

7474 pts/1    S      0:00 sleep 100000

kill 7474
```
14. Mata el bash actual.
```
ps
6966 pts/1    00:00:00 bash
kill -9 6966
```
15. Llista tots els processos mingetty i mata'ls de cop tots usant una sola ordre kill.
```
pidof mingetty (nom exacte)
pgrep -l mingetty (expressió)
```
16. Genera 3 processos sleeep 10000 i mata'ls tots de cop usant killall.
```
sleep 100000 &
sleep 100000 &
sleep 100000 &
killall sleep
```
17. Executa tres ordres sleep en segon pla i llista els treballs.
```
sleep 100000 &
sleep 100000 &
sleep 100000 &
jobs  (mostra els processos en execucio en segon pla d'aquesta sessió)

[1]   Running                 sleep 100000 &
[2]-  Running                 sleep 100000 &
[3]+  Running                 sleep 100000 &
```
18. Inicia l'edició d'un fitxer amb vi i deixa'l suspès d'execució en segon pla. Mostrar els treballs.
```
vi hola.txt
^Z
jobs

[1]   Running                 sleep 100000 &
[2]   Running                 sleep 100000 &
[3]-  Running                 sleep 100000 &
[4]+  Stopped                 vi hola.txt
```
19. Mata el segon dels treballs (un sleep).
```
kill %2
jobs

[1]   Running                 sleep 100000 &
[2]-  Terminated              sleep 100000
[3]+  Running                 sleep 100000 &

```
20. Passa a primer pla el primer dels treballs (un sleep), i mata'l amb ctrl+c.
```
fg %1
^C
```
21. Passa a primer pla el treball més recent. Quin és. Acabar. 
```
fg
És el [3]
^C
```
22. Llistar tota l'estructura de directoris partint de l'arrel. Que no es generin missatges d'error i enviar la sortida a null (no volem desar res és només per fer-lo treballar!). Un cop iniciat aturar el proces. Llistar els treballs.
```
tree / &> /dev/null
^Z
jobs

[1]+  Stopped                 sudo tree / &> /dev/null
```
23. Reanudar l'execució del tree anterior en segon pla.
```
bg

[1]+ sudo tree / &> /dev/null &

jobs

[1]+  Done                    sudo tree / &> /dev/null
```
24. Executar l'ordre que monitoritza els processos. Llistar-los per prioritat.
```
top
```
25. Executar l'ordre vmstat. Descriu almenys tres dels elements dels que informa.
```
vmstat

procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 0  0      0 11247780 189428 2502596    0    0   248    90  539 1406  6  2 91  1  0

memory:
    swpd: quantitat de memòria swap utilitzada
    free: quantitat de memòria disponible.
    buff: quantitat de memòria utilitzada al buffer
    cache: quantitat de memòria utilitzada de cache

system:
    in: número de interrupcions per segon.
    cs: número de "context switches" per segon.

CPU:
    us: temps utilitzat carregant codi no associat al kernel.
    sy: temps utilitzat carregant codi del kernel
    id: temps desocupat (idle.)
    wa: temps esperant en IO (input/output)
    st: temps robat (stolen) d'una màq. virtual.
```
26. Executar l'ordre free i descriure la informació que mostra.
```
free

               total        used        free      shared  buff/cache   available
Mem:        16261716     2317048    11249100     546396     2695568    13061100
Swap:        4881404           0     4881404

    total: memòria instal·lada.
    used: memòria utilitzada.
    free: memòria sense utilitzar.
    shared: memòria utilitzada majorment per tmpfs (fitxers temporals?)
    available: memòria disponible.
```
27. Digues quanta estona fa que el sistema està engegat ininterrompudament.
```
uptime

08:16:57 up 13 min,  1 user,  load average: 1.07, 0.86, 0.51
```