******************************************************************************
  # M01-ISO Sistemes Operatius
  ## UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema informàtic
******************************************************************************
  ## A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  ### Exercici 11: pipes, redireccionaments i variables
- pipes
- redireccionamnets
- variables
******************************************************************************

==============================================================================
### Ordre: pipes i redireccion
==============================================================================
00. situar-se al directori actiu /tmp/m01 i realizat TOTES les ordres des d'aquest directori (usuari hisx..
```
    $ cd /tmp/m01
```
01. llistar el número major i el número menor dels dispositius corresponents a la entrada estàndard, sortida estàndard i d'error. Seguir el lnk fins identificar el device real on esta lligat
```
    ls -l /dev/std*

    lrwxrwxrwx 1 root root 15 Jan 17 19:17 /dev/stderr -> /proc/self/fd/2
    lrwxrwxrwx 1 root root 15 Jan 17 19:17 /dev/stdin -> /proc/self/fd/0
    lrwxrwxrwx 1 root root 15 Jan 17 19:17 /dev/stdout -> /proc/self/fd/1
```
02. Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http.
```
    grep http /etc/services > http.txt
```
03. Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http però que al mateix temps es mostri per pantalla.
```
    grep http /etc/services | tee http.txt
```
04. Desar en un fitxer de nom ftp.txt el llistat de tots els serveis que contenen la cadena ftp ordenats lexicogràficament. La sortida s'ha de mostrar simultàniament per pantalla
```
    grep ftp /etc/services | sort | tee ftp.txt
```
05. Idem exercici anterior però mostrant per pantalla únicament quants serveis hi ha.
```
    grep ftp /etc/services | sort | tee ftp.txt | wc -l
```
06. Idem anterior però comptant únicament quants contenen la descrició TLS/SSL
```
    grep ftp /etc/services | sort | tee ftp.txt | grep SSL | wc -l
```
07. Llista l'ocupació d'espai del directori tmp fent que els missatges d'error s'ignorin.
```
    du -h /tmp/ 2> /dev/null
```
08. Idem anterior desant el resultat a disc.txt i ignorant els errors.
```
    du -h /tmp/ > disc.txt 2> /dev/null
```
09. Idem enviant tota la sortida (errors i dades. al fitxer disc.txt
```
    du -h /tmp/ &> /dev/null
```
10. Afegir al fitxer disc.txt el sumari de l'ocupació de disc dels directoris /boot i /mnt. Els errors cal ignorar-los
```
    du -h /boot/ /mnt >> disc.txt 2> /dev/null
```
11. Anomana per a què serveixen les variables:
```
	HOME: directori home de l'usuari actiu. 
    PWD: directori actiu.
    UID: el número d'usuari
    EUID: el número d'usuari effectiu
    HISTFILE: nom del fitxer on s'emmagatzema l'historial d'ordres.
    HISTFILESIZE: el nombre màxim de linees contingudes en el fitxer anterior.
    DISPLAY: mostra el número del monitor actual?
    SHELL: ruta del executable del bash
	HOSTNAME: mostra el nom de la màquina
    HOSTTYPE: mostra tipus d'arquitectura
    LANG: idioma.
    PATH: la ruta on s'emmagatzemen les ordres externes.
    PPID: el PID del procés pare del shell.
    PS1: el prompt primari.
    PS2: el prompt secondari, apareix quan hi ha hagut un error de sintaxis.
    TERM: mostra quin terminal d'emulació s'utilitza per mostrar caràcters a la pantalla.
    USER: mostra l'usuari actual.
```
12. Assigna a la variable NOM el teu nom complert (nom i cognoms. i assegura't que s'exporta als subprocessos.
```
    export NOM="Jordi Jubete"
```
13. Assigna el prompt un format que mostri la hora, la ruta absoluta del directori actiu i el nom d'usuari.
```
    PS1="\T-\w-\u:\$"
```
14. Assigna al prompt el format on mostra el nom d'usuari, de host, el número d'ordre i el número en l'històric d'ordres.
```
    PS1="\u@\h \# \!"
```
15. Compta quants usuaris hi ha connectats al sistema actualment.
```
    who | wc -l
```
16. Llista quins usuaris hi ha connectats al sistema i quantes connexions tenen establertes. Cal mostrar el login i el numero de connexions per a cada usuari ordenat de major a menor nombre de connexions.
```

```