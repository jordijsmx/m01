******************************************************************************
  # M01-ISO Sistemes Operatius
  ## UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema informàtic
******************************************************************************
  ## A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  ### Exercici 14: exercicis de comandes:
- path
- alias
- valors de retorn
- expansions
- command substitution
******************************************************************************
------------------------------------------------------------------------
### alias
------------------------------------------------------------------------
01. Mostrar els alies actualment definits
```
    alias
```
02. Fer un alias anomenat meu-data que mostri la data i el calendari.
```
    alias meu-data='date; cal'
```
03. Fer una alias de la ordre uname de manera que sempre mostri tota la 
    informació possible.
    Observar amb which quina és l'ordre associada a uname ara.
```
    alias uname='uname -a'

    type -a uname
```
04. Fer un alias de tree que llisti únicament els directoris.
    observar amb which l'ordre associada a tree.
```
    alias treedir='tree -d'

    type -a treedir
```
05. Definir un alias permanentment a l'usuari que s'anomeni quisoc i 
    mostri per pantalla el uid, gid i el n'hom d'usuari.
```
    echo "alias quisoc='echo $UID $GID $USER'" >> ~/.bashrc

    s'ha de reiniciar la sessió o obrir un bash nou
```
06. Definir un alias permanentment per a tots els usuaris del sistema 
    anomenat sistema que mostra la data, la informació complerta de 
    uname i tota la informació descriptiva de la cpu.
```
    echo "alias system='date; uname -a; lscpu'" >> /etc/bash.bashrc

    s'ha de reiniciar la sessió o obrir un bash nou
```
------------------------------------------------------------------------
### PATH: ordres externes i internes
------------------------------------------------------------------------
07. Mostrar el PATH actualment definit
```
    echo $PATH
```
08. Eliminar el path (abans fer-ne una còpia a la variable OLDPATH. i 
    observar què passa en executar una ordre interna i una externa.
```
    OLDPATH=$PATH

    PATH=

    les internes funcionen i les externes no, ja que no hi ha res al PATH.
```
09. Llistar l'estat de les ordres internes per obsevar si estan 
    activades o desactivades.
```
    enable -a
```
10. Mostrar la pàgina de manual de les ordres internes
```
    man builtins (no funciona)
```
11. Mostrar l'ajuda de la ordre interna fg.
```
    help fg
```
12. Crear una versió pròpia de l'ordre cal (per exemple que faci un ls., 
    i assignar-li precedència respecte a l'ordre del sistema. És a dir,
    fer que la versió de l'ordre de l'usuari s'executi en lloc de la del
    sistema.
```
    cp /bin/ls ~/bin/cal

    PATH=~/bin:$PATH
```
13. Crear una ordre pròpia local de l'usuari anomenada meu-data (en 
    realitat és la ròpia ordre date..
```
    cp /bin/date ~/bin/meu-data
```
14. Elimina el directori local de l'usuari del PATH i executa l'ordre 
    meu-data. Què cal fer?
```
    PATH=/usr/bin:/bin:/usr/local/games:/usr/games
 
    Utilitzant ruta absoluta o relativa del fitxer executable.

    ~/bin/meu-data
```
------------------------------------------------------------------------
### Valors de retorn i compound commands
------------------------------------------------------------------------
15. Executa l'ordre free i comprova el valor de retorn d'execució.
```
    free

    echo $?
    0
```
16. Executa l'ordre du -s i comproba el valor retornat.
```
    du -s

    echo $?
    0
```  
17. Executa l'ordre date --noval i observa el codi de retorn.
```
    date --noval

    echo $?
    1
```
18. Executa l'ordre nipum-nipam i observa el valor de retorn.
```
    nipum-nipam

    echo $?
    127
```
19. Crear dins de tmp un directori de nom dirprova. Únicament si l'ordre
    funciona mostrar per pantalla un missatge dient "ok directori creat"
```
    mkdir dirprova && echo "ok directori creat"
```
20. Crear dins de tmp un directori de nom dirprova. Únicament si l'ordre
    falla mostrar per pantalla el codi d'error de l'ordre i un missatge
    dient "ERR: el directori no s'ha pogut crear".
```
    mkdir dirprova || echo $? && echo "ERR: el directori no s'ha pogut crear"
```

------------------------------------------------------------------------
### Expansions
------------------------------------------------------------------------
21. Mostra el resultat de multiplicar 10 per 15 i restar-n'hi 5
```
    echo $((10*15-5))
``` 
22. Assigna a la variable num el valor de dividir 40 entre 4?
```
    num=$((40/4))
```
23. Assigna a la variable num el valor equivalent al doble del uid de 
    l'usuari
```
    num=$(($UID*2))
```
24. Assigna a la variable CASA el home de l'usuari usant tilde expansion.
```
    CASA=~
```
25. Mostra el home de l'usuari root i el de l'usuari pere
```
    echo ~root ~pere
```
26. Assigna a la variable FITS el nom de tots els fitxers del directori 
    actual
```
    FITS=$(ls)
```
27. Assigna a la variable fits el nom de tots els fitxers acabats en txt
    del directori actual.
```
    FITS=*
    fits=$(ls *.txt)
```
28. Mostra per pantalla els noms noma, nomb, nomc, nomd
```
    echo nom{a,b,c,d}
```
29. Crear els directoris /tmp/dirprova/dir1 fins a dir10 de cop.
```
    mkdir -p /tmp/dirprova/dir{1..10}
```
30. Crear dins de /tmp/dirprova els següents fitxers buits: carta-1.txt,
    carta-a.txt...fins a carta-e.txt, carta-10.txt... fins a carta-15.txt.
```
    touch carta-{1,{a..e},{10..15}}.txt
```
30. Mostrar per pantalla els següents noms: informe1.txt, informe5.txt, 
    carta-a.txt...fins a carta-e.txt, treball.inf, dossier10... fins 
    dossier22.
```
    echo informe{1,5}.txt carta-{a..e}.txt treball.inf dossier{10..22}
```
------------------------------------------------------------------------
### Command substitution
------------------------------------------------------------------------
31. Assigna a la variable DATANOW la hora actual.
```
    DATANOW=$(date '+%H:%M')
```  
32. Assigna a la variable la data en el moment en que es visualitza.
```
    saltat, mal plantejat
```
33. llista el nom del paquet al que pertany l'ordre executable fsck.
```
    dpkg -S /sbin/fsck
    util-linux: /sbin/fsck
```
34. Executa un finger de l'usuri actual usant whoami.
```
    finger $(whoami)
```
35. Executa l'ordre que indica el tipus de fitxer que són cada un dels 
    executables del paquet al que pertany l'ordre fsck
```
    file $(dpkg -L util-linux | grep bin.
```
36. Assihna a la variable FORMATS el nom dels fitxers executables que
    permeten formatar sistemes de fitxers.
```
    FORMATS=$()
```