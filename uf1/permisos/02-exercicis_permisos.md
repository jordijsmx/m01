﻿# Exercicis de permisos (02)

Conceptes clau:
* Permisos bàsics. 
* Elements d’una entrada de directori
* Diferència entre fitxers i directoris.
* Chmod octal i simbòlix
* Chown / chgrp
* Umask
* Permisos avançats fitxers
* Permisos avançats directoris

Ordres a treballar:
* ls -i 
* stat
* chmod
* chown
* chgrp
* umask

________________________________________________________________________
## Exercicis
________________________________________________________________________

### Primera Part (global)
1. Crea el directori /tmp/m01 amb permisos totals per a tot (777).
```
mkdir /tmp/m01
chmod 777 /tmp/m01
```
2. Crea els usuaris pere, pau i anna.
```
sudo useradd pere
sudo useradd pau
sudo useradd anna
```
3. Crea els grups basquet, tenis i futbol.
```
sudo groupadd basquet
sudo gruopadd tenis
sudo groupadd futbol1
```
4. Assigna a pere els grups futbol i tenis.
```
sudo usermod -aG futbol1,tenis pere
```
5. Assigna a anna al grup futbol.
```
sudo usermod -aG futbol1 anna
```
6. Verifica amb id cada usuari.
```
id USER
```

### Segona Part (fitxers)
7. Com a usuari pere crea dins de /tmp/m01 el fitxer pereinfo.pdf.
```
su - pere
date > /tmp/m01/pereinfo.pdf 
```
8. Mostra la unask de pere i verifica els permisos de pereinfo.pdf amb la umask.
```
umask
022
ls -la /tmp/m01/pereinfo.pdf
-rw-r--r-- 1 pere pere 32 Nov 21 08:37 /tmp/m01/pereinfo.pdf
```
9. Com a usuari pere estableix a pereinfo.pdf els permisos que el propietari ho pot fer tot, el grup mirar i altres res.
```
chmod 640 pereinfo.pdf
```
10. Com a usuària anna intenta veure el contingut del fitxer pereinfo.pdf. Es pot?.
```
su - anna
cat pereinfo.pdf
cat: pereinfo.pdf: Permission denied
```
11. Com a usuari pau intenta veure el contingut del fitxer pereinfo.pdf. Es pot?.
```
su - pau
cat pereinfo.pdf
cat: pereinfo.pdf: Permission denied
```
12. Com a usuari pere canvia el grup propietari del fitxer pereinfo.pdf establint que sigui futbol.
```
su - pere
chgrp futbol1 pereinfo.pdf
chown :futbol1 pereinfo.pdf
-rw-r-----  1 pere      futbol1    32 Nov 21 08:37 pereinfo.pdf
```
13. Com a usuària anna intenta veure el contingut del fitxer pereinfo.pdf. Es pot?. Perquè?
```
su - anna
cat pereinfo.pdf
Mon 21 Nov 2022 08:37:52 AM CET
```
_Pot veure el contingut perque anna pertany al grup futbol1_

14. Com a usuari pau intenta veure el contingut del fitxer pereinfo.pdf. Es pot?. Perquè?
```
su - pau
cat pereinfo.pdf
cat: pereinfo.pdf: Permission denied
```
_No pot veure el contingut perque pau no es ni propietari del fitxer ni pertany al grup futbol1_

### Tercera Part (directoris)
15. Com a usuari pere crea un directori dins de /tmp/m01 anomenat peredir. 
```
su - pere
mkdir peredir
```
16. Com a usuari pere crea dins de peredir un fitxer anomenat perefile.txt.
```
cal > peredir/perefile.txt
```
17. Observa la umask de pere i verifica que s’ha aplicat en els permisos del directori peredir creat.
```
umask
022
ls -la peredir/perefile.txt 
-rw-r--r-- 1 pere pere 184 Nov 21 08:50 peredir/perefile.txt
```
18. Com a usuari pere assigna al directori peredir els permisos totals pel propietari, de navegació pel grup i cap per altres.
```
chmod 750 peredir/
ls -ld peredir
drwxr-x--- 2 pere pere 4096 Nov 21 08:50 peredir
```
19. Com a usuària anna intenta veure el contingut del directori peredir. Es pot?.
```
su - anna
cd peredir
-bash: cd: peredir/: Permission denied
```
20. Com a usuari pau intenta veure el contingut del directori peredir. Es pot?.
```
su - pau
cd peredir
-bash: cd: peredir/: Permission denied
```
21. Com a usuari pere canvia el grup propietari del directori peredir de manera que passi a ser del grup futbol.
```
su - pere
chgrp futbol1 peredir/
```
22. Com a usuària anna intenta veure el contingut del directori peredir. Es pot?. Perquè?
```
su - anna
cd peredir/
```

_Pot veure el contingut del directori ja que és del grup futbol1 i peredir té permis d'accés per al grup._

23. Com a usuari pau intenta veure el contingut del directori peredir. Es pot?. Perquè?
```
su - pau
cd peredir/
-bash: cd: peredir/: Permission denied
```
_No pot accedir al directori ja que no té cap permis a others._

24. Pot la usuaria anna crear un fitxer dins de peredir? Perquè?
```
su - anna
ls > annafile.pdf
-bash: annafile.pdf: Permission denied
```
_No pot crear un fitxer a peredir perque només hi ha permisos d'accés a grup._

25. Pot la usuària anna modificar el contingut del fitxer perefile.txt? Perquè?.
```
vim perefile.txt
```
_No pot modificar el contingut ja que el fitxer nomes té permisos de lectura._

### Quarta Part (SetGID)
26. Com a usuari pere assigna el directori peredir al grup futbol.
```
su - pere
chgrp futbol1 peredir
```
27. Com a usuari pere estableix al directori peredir permisos totals per a propietari i grup i de navegació per a altres. Activa també el SetGID.
```
 chmod 2775 peredir/
```
28. A quin grup pertany el fitxer perefile.txt de dins de peredir?
```
ls -la peredir/
-rw-r--r-- 1 pere  pere     184 Nov 23 21:09 perefile.txt
```
29. Com a usuaria anna crea un fitxer dins de peredir anomanat annafile.txt. 
```
su - anna
ls > annafile.txt
```
30. A quin grup pertany el fitxer annafile.txt? Perquè?
```
ls -la
-rw-rw-r-- 1 anna  futbol1   26 Nov 23 21:26 annafile.txt
```
_Pertany al grup **futbol1** ja que el directori peredir té el **SetGID** activat i tots els fitxers creats després de l'activació **hereten** el grup del directori._

31. Pot l’usuari pau crear un fitxer dins de peredir? Perquè?
```
su -pau
ls > paufile.txt
-bash: paufile.txt: Permission denied
```
_No pot perquè **peredir** només té permís d'accés per a **others**, sense escriptura i pau no pertany al grup futbol1._

### Cinquena Part (Sticky Bit)
32. Com a usuari pau crea un fitxer anomenat pauinfo.pdf dins de /tmp/m01.
```
su - pau
ls > pauinfo.pdf
```
33. Com a usuaria anna esborra el fitxer d’en pau pauinfo.txt. Es pot? Perquè?
```
su - anna
rm pauinfo.pdf
```
_Si que pot perque el directori m01 té **permisos totals** per a tots._ 

34. Amb el teu usuari activa l’sticky bit al directori /tmp/m01.
```
chmod +t /tmp/m01
```
35. Com a usuari pau crea un fitxer anomenat pauinfo.pdf dins de /tmp/m01.
```
su - pau
ls > pauinfo.pdf
```
36. Com a usuaria anna esborra el fitxer d’en pau pauinfo.txt. Es pot? Perquè?
```
su - anna
rm pauinfo.pdf
rm: cannot remove 'pauinfo.pdf': Operation not permitted
```
_No es pot ja que l'**sticky bit** protegeix contra l'esborrat de fitxers. Només el propietari del fitxer pot esborrar-lo._

### Sisena Part (umask)
37. Usant l’usuari pere estableix la màscara de permisos perquè els fitxers i directoris siguin de control total pel propietari, de lectura pel grup i per altres.
```
su - pere
umask 022
```
38. Verifica-ho crear/esborrant un fitxer i un directori.
```
ls > prova.txt
mkdir prova
ls -la
drwxr-xr-x  2 pere  pere    4096 Nov 23 21:45 prova
-rw-r--r--  1 pere  pere      43 Nov 23 21:45 prova.txt
```
39. Usant l’usuari pere estableix la màscara de permisos perquè els fitxers i directoris siguin de control total pel propietari i res per a grups i altres. 
```
umask 077
```
40. Verifica-ho crear/esborrant un fitxer i un directori.
```
ls > prova.txt
mkdir prova
ls -la
drwx------  2 pere  pere    4096 Nov 23 21:46 prova
-rw-------  1 pere  pere      43 Nov 23 21:46 prova.txt
```

### Setena Part ( octal, simbòlic)
41. Com a usuari pere copia el fitxer de l’ordre executable ls dins de /tmp/m01 i l’anomenes myls. Executar-la fent ./myls.
```
cp /usr/bin/ls myls
./myls
myls  pauinfo.pdf  peredir  pereinfo.pdf
```
42. Observa els permissos que té. Té activat el permís executable?
```
ls -la
-rwx--x--x  1 pere  pere    138208 Nov 23 21:48 myls
```
43. Com a usuari pere desactiva simbòlicament el permís executable.
```
 chmod -x myls
 -rw-------  1 pere  pere    138208 Nov 23 21:48 myls
```
44. Com a usuari pere executar ./myls. Funciona?
```
./myls
-bash: ./myls: Permission denied
```
45. Com a usuari pere activar el bit executable només del propietari, amb simbòlic, al fitxer myls.
```
chmod u+x myls
```
46. Com a usuari pere establir al grup els mateixos permisos que al propietari, amb simbòlic, al fitxer myls.
```
chmod g=u myls
```
47. Com usuari pere amb simbòlic desactivar la w al propietari, grup i altres del fitxer myls.
```
chmod a-w myls
```
48. Com a usuari pere activa amb simbòlic el setuid del fitxer myls.
```
chmod u+s myls
```
49. Com a usuari pere activa amb simbòlic l’sticky bit del fitxer myls (encara que no serveixi per res).
```
chmod +t myls
```
50. Com a usuari pere estableix els permisos rw- en simbòlic al propietari, grup i altres al fitxer myls (tot i que per norma general això ho fem amb octal).
```
chmod a=rw myls
```

### Vuitena Part ( Setuid, Setgid)
51. Com a usuari pere copia el fitxer executable passwd dins del directori /tmp/m01 amb els nom mypasswd.
```
 cp /usr/bin/passwd mypasswd
```
52. Com a usuari pere observa els permisos de mypasswd.
```
ls -la mypasswd
-rwsr-xr-x  1 pere pere 63960 Nov 28 08:33 mypasswd
```
53. Com a usuari pere desactiva el setuid de mypasswd amb simbòlic.
```
chmod u-s mypasswd
```
54. Com a usuari pere activa el setuid de mypasswd amb octal.
```
chmod 4755 mypasswd 
```
55. Com a usuari pere copia el fitxer executable write dins del directori /tmp/m01 amb els nom mywrite.
```
cp /usr/bin/write.ul mywrite
```
56. Com a usuari pere observa els permisos de mywrite.
```
ls -la /usr/bin/write
```
57. Com a usuari pere desactiva el setgid de mywrite amb simbòlic.
```
chmod g-s mypasswd
```
58. Com a usuari pere activa el setgid de mywrite amb octal.
```
chmod 2755 mywrite 
```
59. En una sessió de consola de text de l’usuari pere i una de la usuària anna fes un write per escriure text de pere a anna.
```

```
60. Elimina el setgid de l’ordre del sistema write. Quin usuari has de ser?
```

```
61. Intenta que pere facci un write a anna. Es pot?
```

```
62. Restableix els permisos de l’ordre write.
```

```