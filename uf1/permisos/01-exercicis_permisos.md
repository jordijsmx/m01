﻿# Exercicis de permisos (01)
## 	Conceptes clau:
* Permisos bàsics. 
* Elements d’una entrada de directori
* Diferència entre fitxers i directoris.
* Chmod octal i simbòlic
* Chown / chgrp
* Umask


* Permisos avançats fitxers
* Permisos avançats directoris
## Ordres a treballar:
 * ls -i 
 * stat
 * chmod
 * chown
 * chgrp
 * umask
 * Exercicis
________________
### Primera Part
1. Escriu l’ordre que llista el nom i inode dels elements del directori actiu.
```
ls -i
```	
2. Quina informació hi ha realment desada en una entrada de directòri?
```
El nom i l'inode.
```	
3. Descriu que és un inode.
```
És el número identificatiu on està desada la informació dels atributs d'un element.
```	
4. Mostra la informació tècnica (contingut de l’inode) del fitxer carta.txt del directori actiu.
```
stat carta.txt
```	
5. Escriu en octal i simbòlic els tres casos clàssics de permisos a fitxers.
```
6 - rw- (llegir i modificar)
4 - r-- (llegir) 
0 - --- (res)
```	
6. Escriu en octal i simbòlic els tres casos clàssics de permisos a directoris.
```
7 - rwx (control total)
5 - r-x (accedir)
0 - --- (res)
```	

### Segona Part

7. Des de /var/tmp estableix en octal al fitxer /tmp/carta.txt permisos de lectura i escritura per tothom.
```
chmod 666 /tmp/carta.txt
```	
8. Des de /tmp estableix en octal al directori /var/tmp/operatius permisos de lectura i escritura per tothom.
```
chmod 777 /var/tmp/operatius
```	
9. Des de /vat/tmp estableix en octal al fitxer /var/tmp/informe.pdf permisos totals al propietari, de lectura pel grup i cap per a altres.
```
chmod 640 informe.pdf
```	
10. Des del directori /var/tmp estableix en octal al directori /var/tmp/operatius permisos totals al propietari, de lectura al grup i cap a altres.
```
chmod 750 operatius
```	
11. Elimina del fixer executable /tmp/practica.py totes les x que tingui. Des del teu home.
```
chmod -x /tmp/practica.py
```	
12. Estableix al directori /vat/tmp/operatius que els permisos de others siguin els mateixos que els que té per a group. El directori actiu és /tmp.
```
chmod o=g /var/tmp/operatius
```	

### Tercera Part

13. L’usuari joan propietari del fitxer /tmp/carta vol que aquest fitxer passi a ser propietat de l’usuaria anna. Des del directori actiu /tmp.
```
Només pot canviar de propietari root.
```	
14. L’usuari joan vol que el directori /var/tmp/operatius (del qual n’és propietari) passi a ser del grup practica (que també en forma part).
```
chown :practica /var/tmp/operatius
```	
15. Si la màscara de permisos és 002 quins són els permisos per defecte de directoris.
```
775
```	
16. Si la màscara de permisos és 002 quins són els permisos per defecte de fitxers?.
```
664
```	
17. Estableix la màscara per defecte per tal de que el propietari tingui accés total, el grup de lectura i a altres cap permís.
```
umask 027
```	

### Quarta  Part

18. Activa al directori /tmp/master el setgid. Directori actual el teu home.
```
chmod g+s /tmp/master
chmod 2755 /tmp/master
```	
_Ens hem d'assegurar que tinguem els permisos d'accés al directori per a que funcioni correctament. (r - x)_

19. Des del teu home estableix el setuid al fitxer executable /tmp/practica.py permeten l’accés total al propietari, execució al grup i a altres.
```
chmod 4755 /tmp/practica.py
```	
20. Què fa el setuid en un directori?
```
No té sentit aplicar aquest permís a directoris
```	
21. Estableix al directori /tmp/compartit (des del directori /tmp) els permisos per fer d’aquest directori un espai compartit per tothom però amb protecció contra esborrat d’altres.
```
chmod 1777 /tmp/compartit
```	
_Ens hem d'assegurar que estiguin activats els permisos totals al directori per a que funcioni correctament. ( r w x )_

22. Si els permisos que establim en octal a un fitxer són 1766 com és representen en simbòlic?
```
rwx rw- rwT
```	
23. Si els permisos que establim a un fitxer són 6765 com és repersentem en simbòlic?
```
rws rwS r-x
```	