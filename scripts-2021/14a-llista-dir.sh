#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 14a-llista-dir.sh

# validar que existeix un arg

ERR_NARGS=1
ERR_NOTDIR=2

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 directori"
   exit $ERR_NARGS
fi

# dir és un directori

dir=$1

if [ ! -d $dir ]; then
   echo "Error: $1 no és un directori"
   echo "Usage: $0 directori"
   exit $ERR_NOTDIR
fi
# fer un ls del directori si ho és

ls -lh $1
exit 0


