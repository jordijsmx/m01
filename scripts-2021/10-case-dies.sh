#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022


# 1 validar arguments



# 2 validar si help



# 3 validar arg pren valors [1-12]

mes=$1

if ! [ $mes -ge 1 -a $mes -le 12 ]
then
   echo "Error, mes $1 no vàlid"
   echo "Mes pren valors del [1-12]"
   echo "Usage: $0 mes"
   exit $ERR_ARGVL
fi

# 4 xixa: determinar numero dies del mes

case "$mes" in
   "2")
      dies=28;;
   "4"|"6"|"9"|"11")
      dies=30;;
   *)
      dies=31;;
esac
echo "El mes és: $mes, té $dies dies"
exit 0


