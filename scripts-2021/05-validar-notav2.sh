#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 05-validar-notav2.sh nota
# Validar nota: suspès, aprovat
# ---------------------------------

ERR_NARGS=1
ERR_NOTA=2
# 1) Si num args no es correcte plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 nota"
   exit $ERR_NARGS
fi

# 2) Validar rang nota (neguem la condició)

if ! [ $1 -ge 0 -a $1 -le 10 ]
then
   echo "Introdueix una nota vàlida."
   echo "La nota és entre 0 i 10."
   echo "Usage: $0 nota"
   exit $ERR_NOTA
fi

# Xixa
