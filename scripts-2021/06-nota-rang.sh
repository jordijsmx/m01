# scripts-2021

## Curs 2021-2022

## Jordi Jubete hisx1
# -----------------------

ERR_NARGS=1
ERR_NOTA=2
# 1) Si num args no es correcte plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 nota"
   exit $ERR_NARGS
fi

# 2) Validar rang nota (neguem la condició)

if ! [ $1 -ge 0 -a $1 -le 10 ]
then
   echo "Introdueix una nota vàlida."
   echo "La nota és entre 0 i 10."
   echo "Usage: $0 nota"
   exit $ERR_NOTA
fi

# Xixa

nota=$1

if [ $nota -lt 5 ] 
then
   echo "La nota és $nota. Suspès."
elif [ $nota -lt 7 ]
then
   echo "La nota és $nota. Aprovat."
elif [ $nota -lt 9 ]
then
   echo "La nota és $nota. Notable."
else
   echo "La nota és $nota. Excel·lent."
fi
exit 0
