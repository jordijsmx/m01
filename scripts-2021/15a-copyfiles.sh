#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 15a-copyfiles.sh fileorigen dirdesti

# validar que hi ha 2 arguments

ERR_NARGS=1
ERR_NOTFILE=2
ERR_NOTDIR=3

if [ $# -ne 2 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 file directori"
   exit $ERR_NARGS
fi

# validar que el primer argument és un file

file=$1

if [ ! -f $file ]; then
   echo "$file no és un fitxer."
   exit $ERR_NOTFILE
fi

# segon és un dir existent

desti=$2

if [ ! -d $desti ]
then
   echo "Error: $desti no és un directori."
   exit $ERR_NOTDIR
fi

# xixa

cp $file $desti
exit 0
