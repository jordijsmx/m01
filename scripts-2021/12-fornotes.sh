#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 12-fornotes.sh
# Programa que rep un argument i diu la nota segons el seu rang
# -----------------------------

ERR_NARGS=1
ERR_NOTA=2

# validar que hi hagi un argument

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 nota[...]"
   exit $ERR_NARGS
fi

# nota entre 0-10 i bucle for

for nota in $*
do

# ERROR RECUPERABLE: si es produeix un error en la nota
# el programa continua. L'error es mostra per STDERR
# OBLIGATORI ">>"

if ! [ $nota -ge 0 -a $nota -le 10 ]
then
   echo "Error: nota $nota no vàlida [0,10]" >> /dev/stderr  

# per cada nota dius is suspès, aprovat, not o ex

elif [ $nota -lt 5 ]
then
   echo "La nota és $nota. Suspès."
elif [ $nota -lt 7 ]
then
   echo "La nota és $nota. Aprovat."
elif [ $nota -lt 9 ]
then
   echo "La nota és $nota. Notable."
else
   echo "La nota és $nota. Excel·lent."
fi
done
exit 0



