#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 14b-llista-dir.sh

# validar que existeix un arg

ERR_NARGS=1
ERR_NOTDIR=2

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 directori"
   exit $ERR_NARGS
fi

# dir és un directori

dir=$1

if [ ! -d $dir ]; then
   echo "Error: $1 no és un directori"
   echo "Usage: $0 directori"
   exit $ERR_NOTDIR
fi

# per cada element del directori
# dir si es regular, link o altres
# IMPORTANT, S'HAN D'UTILITZAR RUTES ABSOLUTES!!

num=1
llista_dir=$(ls $dir)

for nom in $llista_dir
do

if [ -h "$dir/$nom" ]; then
   echo "$num: $nom és un link"
elif [ -d "$dir/$nom" ]; then
   echo "$num: $nom és un directori"
elif [ -f "$dir/$nom" ]; then
   echo "$num: $nom és un regular file"
else
  echo "$num: $nom és una altra cosa"
fi
   ((num++))
done
exit 0


