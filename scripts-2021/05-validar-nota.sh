#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022

# 05-validar-nota.sh nota
# Validar nota: suspès, aprovat
# ---------------------------------
# 1) Si num args no es correcte plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 nota"
   exit 1
fi

# 2) Validar rang nota

nota=$1

if [ $1 -le 0 -o $1 -gt 10]
then
   echo "Introdueix una nota vàlida."
   exit 1
fi

# Xixa

if [ $nota -ge 5 -a $nota -le 10 ]
then
   echo "La teva nota és $nota, has aprovat."
else
   echo "La teva nota és $nota, has suspès."
fi
exit 0






# Xixa


