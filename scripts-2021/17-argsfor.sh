#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 17-argsfor.sh -a pere -b marta joan -s -n -i ramon
# options: -a -b -s -n -i
# arguments: pere marta joan ramon

# validar numero d'arguments

ERR_NARGS=1

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 [-a -b -c -d- e -f] arg[...]"
   exit $ERR_NARGS
fi

# xixa

opcions=""
arguments=""

for arg in $*
do
   case $arg in
      "-a"|"-b"|"-c"|"-d"|"-e"|"-f")
          opcions="$opcions $arg";;
      *)
          arguments="$arguments $arg"
   esac
done

echo "Opcions: $opcions"
echo "Arguments: $arguments"

exit 0

