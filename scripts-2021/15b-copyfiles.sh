#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 15a-copyfiles.sh fileorigen dirdesti

# validar que hi ha 2 arguments

ERR_NARGS=1
ERR_NOTFILE=2
ERR_NOTDIR=3

if [ $# -lt 2 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 file directori"
   exit $ERR_NARGS
fi

# separar desti i llista files

llista_files=$(echo $* | sed 's/ [^ ]*$//')
desti=$(($#))

# segon és un dir existent

if [ ! -d $desti ]
then
   echo "Error: $desti no és un directori."
   exit $ERR_NOTDIR
fi

# iterar file a file

for file in $llista_files
do

if [ ! -f $file ]; then
   echo "$file no és un fitxer."
   exit $ERR_NOTFILE
else
 
  cp $file $desti
fi
done
exit 0
