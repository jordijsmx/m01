#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici09.sh
# Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen 
# en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. 
# Si no existeix el mostra per stderr.
# -----------------------------


while read -r line && true
do

  grep "^$line:" /etc/passwd | cut -d: -f1
  
done


exit 0
