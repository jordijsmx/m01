
#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici02.sh
# Mostrar els arguments rebuts línia a linia, tot numerant-los
# -----------------------------



ERR_NARGS=1

# validar que hi hagi arguments

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 arguments"
   exit $ERR_NARGS
fi

# mostrar-los linia a linia

num=1

for arg in $*
do
   echo "$num : $arg"
   ((num++)) 
done
exit 0
