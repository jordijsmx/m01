
#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici03.sh
# Fer un comptador des de zero fins al valor indicat
# per l'argument rebut
# -----------------------------


ERR_NARGS=1

# validar que hi hagi un arguments

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 arguments"
   exit $ERR_NARGS
fi

# xixa

MAX=$1

num=0

while [ $num -le $MAX ]
do
   echo "$num"
   ((num++))
done
exit 0

