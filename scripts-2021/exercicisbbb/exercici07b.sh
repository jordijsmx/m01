# 1) validar nº arguments
ERR_NARG=1
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 usuari[...]"
   exit $ERR_NARG
fi
# 2) validar si el nom d'usuari existeix en el sistema
llista=$(cut -d: -f1 /etc/passwd)
for usuari in $*
do
  echo $llista | grep -Eq '^$usuari$'
  # si no existeix mostra per stderr
  if [ $? -eq 0 ]
  then
    echo $usuari >> /dev/stderr
  else
    echo $usuari
  fi
done
exit 0
