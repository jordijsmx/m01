#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici08.sh
# Rep com a argument noms d'usuari, si existeixen en el sistema (/etc/passwd) mostra el nom
# per stdout. Si no existeix, el mostra per stderr
# -----------------------------

# validar arguments

ERR_NARGS=1

if [ $# -lt 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 usuari"
   exit $ERR_NARGS
fi

# xixa

for user in $*
do 
   
  grep -w "^$user:" /etc/passwd | cut -d: -f1

  if [ $? -eq 0 ];then
   
  echo "L'usuari $user no existeix." > /dev/stderr
  
  else

  echo "L'usuari $user existeix."

  fi

done
exit 0
