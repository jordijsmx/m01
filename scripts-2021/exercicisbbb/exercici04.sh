
#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici04.sh
# Rep com a arguments números de més (un o més)
# i indica per a cada mes rebut quants dies té
# el mes
# -----------------------------


ERR_NARGS=1 
ERR_ARGVL=2 
OK=0 

# validar si hi ha arguments

if [ $# -eq 0 ]
then
   echo "Error, número d'arguments no vàlid."
   echo "Usage: $0 mes"
   exit $ERR_NARGS
fi

# validar si els arguments tenen el valor [1-12]

mesos=$*

for mes in $mesos
do

if ! [ $mes -ge 1 -a $mes -le 12 ]
then
   echo "Error, mes $mes no vàlid, pren el valor [1-12]" 1>&2 
   
# xixa

else

case "$mes" in
   "2")
     dies=28;;
   "4"|"6"|"9"|"11")
     dies=30;;
    *)
     dies=31;;
esac

echo "El mes: $mes, té $dies dies"

fi
done
exit 0

