#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici01.sh
# Mostrar el stdin numerant línia a linia
# -----------------------------


num=1

while read -r linia
do
   echo "$num: $linia"
   ((num++)) 
done
exit 0

