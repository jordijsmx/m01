#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici05.sh
# Mostrar línia a línia l'entrada estandard
# retallant només els primers 50 caràcters
# -----------------------------


while read -r linia
do
   echo "$linia" | cut -c 1-50 
done
exit 0
