#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici06.sh

# Rep com arguments noms de dies setmanals i mostra quants dies eren laborables i quants festius. 

# Si l'argument no és un dia de la setmana genera un error per stderr

# Ex: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
# -----------------------------


ERR_NARGS=1

# validar que hi hagi un arguments

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 arguments"
   exit $ERR_NARGS
fi

# xixa

dies=$*

for tipus in $dies
do

case "$tipus" in
   "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
     echo "El $tipus és laborable";;
   "dissabte"|"diumenge")
     echo "El $tipus és festiu";;
    *)
     echo "El $tipus no és un dia de la setmana" 1>&2 ;;

esac
done
exit 0


