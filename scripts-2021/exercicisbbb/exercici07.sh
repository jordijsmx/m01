#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exercici07.sh
# Processar línia a línia stdin. Si la línia té més de 60 caràcters
# la mostra, si no, no.
# -----------------------------
while read -r line
do
   linia=$(echo -n "$line" | wc -c)
   if [ $linia -gt 60 ] ; then
   echo "$line"
   fi
done
exit 0
