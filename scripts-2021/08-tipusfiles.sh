#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022

# 08-tipusfiles.sh

# 1) Si no hi ha arguments, error

ERR_NARGS=1
ERR_NOEXIST=2

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 file"
   exit $ERR_NARGS
fi


# Xixa

fitxer=$1

if [ -e $fitxer ]; then
   echo "$fitxer no existeix"
   exit $ERR_NOEXIST
elif [ -f $fitxer ]; then
   echo "$fitxer és un regular file"
elif [ -h $fitxer ]; then
   echo "$fitxer és un link"
elif [ -d $fitxer ]; then
   echo "$fitxer és un directori"
else
  echo "$fitxer és una altra cosa"
fi
exit 0
