#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 14d-llista-dir.sh

# validar que existeix un arg

ERR_NARGS=1
ERR_NOTDIR=2

if [ ! $# -gt 0 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 directori"
   exit $ERR_NARGS
fi

# iterar si cada argument és un directori

dir=$*
for arxiu in $*
do
if [ ! -d $arxiu ]; then
   echo "Error: $dir no és un directori"
   echo "Usage: $0 directori"
   exit $ERR_NOTDIR
fi
done

# per cada element del directori
# dir si es regular, link o altres
# IMPORTANT, S'HAN D'UTILITZAR RUTES ABSOLUTES!!

num=1

for nom in $*
do
if ! [ -d $dir ]
then
   echo "Error: $dir no és un directori"
else

llista_dir=$(ls $dir)
echo "dir: $dir"

for nom in $llista_dir
do

if [ -h "$dir/$nom" ]; then
   echo "$num: $nom és un link"
elif [ -d "$dir/$nom" ]; then
   echo "$num: $nom és un directori"
elif [ -f "$dir/$nom" ]; then
   echo "$num: $nom és un regular file"
else
  echo "$num: $nom és una altra cosa"
fi
   ((num++))
done
exit 0


