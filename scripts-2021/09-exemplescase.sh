#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022

# 09-exemplescase.sh
# -------------------------

# dl dt dc dj dv laborable

case $1 in
  "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
      echo "$1 és laborable"
      ;;
  "dissabte"|"diumenge")
      echo "$1 és festiu"
      ;;
  *)
      echo "$1 no és un dia"
esac
exit 0

case $1 in
   [aeiou])
       echo "$1 és una vocal"
       ;;
   [bcdfghjklmnpqrstvwxyz])
       echo "$1 és consonant"
       ;;
   *)
       echo "$1 és una altra cosa"
esac
exit 0

case $1 in
   "pere"|"pau"|"joan")
       echo "és un nen"
       ;;
   "marta"|"anna"|"julia")
       echo "és una nena"
       ;;
   *)
       echo "és indefinit"
       ;;
esac
exit 0
