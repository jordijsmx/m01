#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 19-creadir.sh noudir...


# validar almenys un argument

ERR_NARGS=1
ERR_DIR=2
status=0

if [ $# -lt 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 nomdir..."
   exit $ERR_NARGS
fi

# mkdir no genera cap sortida
# si tot ha anat be, el programa retorna un 0
# si hi ha error d'arguments, retorna un 1
# si no s'ha pogut crear algun directori, retorna un 2

for arg in $*
do

   mkdir $arg &> /dev/null

if [ $? -ne 0 ];then

  echo "Error: no s'ha creat $arg" >&2
  status=$ERR_DIR 

fi
done
exit $status
