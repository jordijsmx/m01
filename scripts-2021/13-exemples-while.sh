
#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 13-exemples-while.sh
# Exemples bucle while
# ----------------------------- 


# numerar stdin linia a linia i passar a majúscules
num=1
while read -r line
do
   echo "$num: $line" | tr 'a-z' 'A-Z'  	
   ((num++))
done
exit 0



# processa stdin fins al token FI
read -r line
while [ "$line" != "FI" ]
do
   echo "$line"
   read -r line
done
exit 0


# processa l'entrada estandard i la mostra numerada
# linia a linia
num=1

while read -r line
do
   echo "$num: $line"
   ((num++)) 
done
exit 0



# processar l'entrada estandard linia a linia
# segueix llegint fins que detecta un FI DE FLUXE
while read -r line
do
   echo $line
done
exit 0




# mostrar arguments amb while
# al fer shift, tots els arguments es desplaçen
# a l'argument anterior

while [ -n "$1" ]
do
   echo  "$1, $#, $*"
   shift
done
exit 0


# compte enrere donat un valor d'argument rebut

MIN=0
cont=$1

while [ $cont -ge $MIN	]
do
  echo "$cont"
  ((cont--))
done
exit 0


# mostrar un comptador del 1 a MAX

MAX=10
num=1

while [ $num -le $MAX ]
do
   echo "$num"
   ((num++))
done
exit 0

