#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# exemples bucle for
# -----------------------------

# llistar logins numerats
# l'unic que hem de canviar és la variable de
# command substitution

num=1
llista_noms=$(cut -d: -f1 /etc/passwd | sort)

for elem in $llista_noms
do
   echo "$num: $elem"
   ((num++))
done
exit 0


# llistar numerats tots els fitxers del dir actiu
# diferent manera de increment!

num=1
llista_noms=$(ls)

for elem in $llista_noms
do 
   echo "$num: $elem"
   ((num++))
done
exit 0



# iterar arguments enumerats
# tot comptador té la inicialització i l'increment

num=1


for arg in $* 
do
   echo "$num: $arg"
   num=$((num+1))
done
exit 0



# iterar per la llista d'arguments
# $@ expandeix els words encara que estiguin encapsulats
for arg in "$@"
do
   echo $arg
done
exit 0


# iterar per la llista d'arguments

for arg in $*
do
   echo $arg
done
exit 0 

# iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
   echo $nom
done
exit 0

# iterar per un conjunt d'elements

for nom in "pere pau marta anna"
do
   echo "$nom"
done
exit 0

# iterar per un conjunt d'elements

for nom in "pere" "pau" "marta" "anna"
do
   echo "$nom"
done
exit 0

