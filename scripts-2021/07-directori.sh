#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022

# 07-directori.sh

# 1) Si no hi ha arguments, error

ERR_NARGS=1
ERR_NOTDIR=2

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 directori"
   exit $ERR_NARGS
fi


# 2) Si no és un directori, error

dir=$1

if [ ! -d $dir ]; then
   echo "Error: $1 no és un directori"
   echo "Usage: $0 directori"
   exit $ERR_NOTDIR
else
   ls $1
fi
exit 0
  
 

# 3) Si és un directori, ls



