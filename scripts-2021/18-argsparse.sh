#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 18-argsparse.sh -a file -b -c -d num -f arg[...]


# validar numero d'arguments

ERR_NARGS=1

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 [-a -b -c -d- e -f] arg[...]"
   exit $ERR_NARGS
fi

# xixa

opcions=""
arguments=""
fitxer=""
num=""

# SHIFT ES SALTA ELS ARGUMENTS DONATS DESPRES DE LES OPCIONS -a I -d

while [ "$1" ]
do

   case $1 in
   "-b"|"-c"|"-e")
     opcions="$opcions $1";;
   "-a")
     opcions="$opcions $1"   
     fitxer=$2
     shift;;
   "-d")
     opcions="$opcions $1"
     num=$2
     shift;;
   *)
     arguments="$arguments $1";;
   esac 
   shift   

done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
echo "Fitxer: $fitxer"
echo "Num: $num"

exit 0
