#! /bin/bash

# @jordijsmx ASIX-M01
# Febrer 2022
# 14b-llista-dir.sh

# validar que existeix un arg

ERR_NARGS=1
ERR_NOTDIR=2

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte."
   echo "Usage: $0 directori"
   exit $ERR_NARGS
fi

# dir és un directori

dir=$1

if [ ! -d $dir ]; then
   echo "Error: $1 no és un directori"
   echo "Usage: $0 directori"
   exit $ERR_NOTDIR
fi

# numerar un a un els noms de fitxers

num=1
llista_dir=$(ls $dir)

for elem in $llista_dir
do
   echo "$num: $elem"
   ((num++))
done
exit 0




